package org.example;

import org.example.service.CashbackService;

public class Main {
    public static void main(String[] args) {
        final CashbackService cashbackService = new CashbackService();
        final int cashback = cashbackService.getCashback(2000, 1);
        System.out.println(cashback);
    }
}
